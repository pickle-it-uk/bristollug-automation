#!/usr/bin/env python3

import os
import os.path
import shutil
import sys
import subprocess
import argparse
import tempfile
import json
import io

script_path = "/opt/bblug-automation"
sender_email_address = "sendmail@bristol.lug.org.uk"
reply_to_addresses = "david@bristol.lug.org.uk"

# Construct the argument parser
ap = argparse.ArgumentParser()

# Add the arguments to the parser
ap.add_argument("-p", "--msgpath", default=script_path, required=False,
  help="path to folder of message files")
ap.add_argument("-f", "--msgfile", default="message.txt", required=False,
  help="filename of file containing email body [txt].")
ap.add_argument("-H", "--msgfilehtml", default="message.html", required=False,
  help="filename of file containing email body [html].")
ap.add_argument("-t", "--msgtmpl", default="message.json", required=False,
  help="filename of email message template.")
ap.add_argument("-d", "--destination", default="destination.json", required=False,
  help="filename containing destination email addresses [json].")
args = ap.parse_args()

#json_formatted = json.dumps(a_string)

message_filestore_path = args.msgpath
messsage_filename_txt = args.msgfile
messsage_filename_html = args.msgfilehtml
message_template_filename = args.msgtmpl
destination_filename = args.destination

#msg_txt_file_path = script_path + "/" + "message.txt"
msg_txt_file_path = message_filestore_path + "/" + messsage_filename_txt
msg_txt_file = open(msg_txt_file_path, "r")
msg_txt_file_data = msg_txt_file.read()
msg_txt_file.close()

#msg_html_file_path = script_path + "/" + "message.html"
msg_html_file_path = message_filestore_path + "/" + messsage_filename_html
msg_html_file = open(msg_html_file_path, "r")
msg_html_file_data = msg_html_file.read()
msg_html_file.close()

#msg_json_tmpl_file_path = script_path + "/" + "message.json"
msg_json_tmpl_file_path = script_path + "/" + message_template_filename
msg_json_tmpl_file = open(msg_json_tmpl_file_path, "r")
msg_json_tmpl_file_data = msg_json_tmpl_file.read()
msg_json_tmpl_file.close()

json_formatted_txt = json.dumps(msg_txt_file_data)
json_formatted_html = json.dumps(msg_html_file_data)

msg_json_tmp_file_data = msg_json_tmpl_file_data.replace("<REPLACEME_TXT>",json_formatted_txt).replace("<REPLACEME_HTML>",json_formatted_html)
#print(msg_json_tmp_file_data)

msg_json_tmp_path = message_filestore_path + "/" + "message.tmp.json"
msg_json_tmp = open(msg_json_tmp_path, 'w')
msg_json_tmp.write(msg_json_tmp_file_data)
msg_json_tmp.close()

destinationJSONFile = "file://" + script_path + "/" + destination_filename
messageJSONFile = "file://" + script_path + "/" + "message.tmp.json"

#subprocess.run(["wimmount", bootWimPath,  "1", bootWimPathMountpoint])
subprocess.run(["aws", "ses", "send-email", "--from", sender_email_address, "--reply-to-addresses", reply_to_addresses, "--destination", destinationJSONFile, "--message", messageJSONFile])


#aws ses send-email --from sendmail@bristol.ug.org.uk --destination file://destination.json --message file://message.tmp.json
