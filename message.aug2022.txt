Hi

Has it been a month already? The next LUG meeting is this Saturday. 

Meetings are now in the hybrid format.

If you want to join the LUG meeting IN PERSON:
We meet at the Ye Shakespeare Pub (details below). The pub has wifi and power sockets, if you want to bring a laptop or other device to use or show.

If you want to join the LUG meeting VIRTUALLY:
...go to the following URL: https://meet.jit.si/BristolLug
and use the following password: lug1458

The best browser to use is: chrome or chromium, but I believe that
firefox works ok.

You can download apps for android and apple phones/tablets at: 
https://meet.jit.si/

FYI:
Meetings are held every on the 4th Saturday of the Month (except December, when it's on the 3rd Saturday).

Date: 4th Saturday (Jan-Nov). 3rd Saturday (Dec).
Time: 14:30 (that's 2.30pm) until 16:30 (that's 4.30pm) (Timezone: GMT/BST).
Location: online (details above).
Location: Ye Shakespeare, 78 Victoria St, Redcliffe, Bristol BS1 6DR.


Regards

-----------------------------------
BristolLUG Mailer
(Sending on behalf of David Fear :: david@bristol.lug.org.uk)
