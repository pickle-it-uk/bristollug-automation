#!/bin/bash

# variables
DESTINATION_DIR="/opt/bblug-automation"
SYSTEMD_DIR="/etc/systemd/system"

# script must be run as root or sudo
if (( $EUID != 0 )); then
    echo "Please run as root or sudo"
    exit
fi

# check if directory exists
if [ -d $DESTINATION_DIR ]
  then 
    echo "/var/log/messages exists."
  else
    mkdir $DESTINATION_DIR
fi

# copy files
cp destination.json message.*.json message_today.*.json message_1hour.*.json message.*.html message.*.txt send-monthly-meeting-reminder.py $DESTINATION_DIR

cp systemd/*.timer systemd/*.service $SYSTEMD_DIR

# change owner & permissions
chown -R root:root $DESTINATION_DIR
chmod +x $DESTINATION_DIR/send-monthly-meeting-reminder.py

# enable systemd timers
systemctl enable send-monthly-meeting-reminder-4thThu.timer
systemctl enable send-monthly-meeting-reminder-4thFri.timer
systemctl enable send-monthly-meeting-reminder-4thSatToday.timer
systemctl enable send-monthly-meeting-reminder-4thSat1Hour.timer
systemctl enable send-monthly-meeting-reminder-3rdThuDec.timer
systemctl enable send-monthly-meeting-reminder-3rdFriDec.timer
systemctl enable send-monthly-meeting-reminder-3rdSatDecToday.timer
systemctl enable send-monthly-meeting-reminder-3thSatDec1Hour.timer

# start systemd timers
systemctl start send-monthly-meeting-reminder-4thThu.timer
systemctl start send-monthly-meeting-reminder-4thFri.timer
systemctl start send-monthly-meeting-reminder-4thSatToday.timer
systemctl start send-monthly-meeting-reminder-4thSat1Hour.timer
systemctl start send-monthly-meeting-reminder-3rdThuDec.timer
systemctl start send-monthly-meeting-reminder-3rdFriDec.timer
systemctl start send-monthly-meeting-reminder-3rdSatDecToday.timer
systemctl start send-monthly-meeting-reminder-3thSatDec1Hour.timer

# get status systemd timers
systemctl status send-monthly-meeting-reminder-4thThu.timer
systemctl status send-monthly-meeting-reminder-4thFri.timer
systemctl status send-monthly-meeting-reminder-4thSatToday.timer
systemctl status send-monthly-meeting-reminder-4thSat1Hour.timer
systemctl status send-monthly-meeting-reminder-3rdThuDec.timer
systemctl status send-monthly-meeting-reminder-3rdFriDec.timer
systemctl status send-monthly-meeting-reminder-3rdSatDecToday.timer
systemctl status send-monthly-meeting-reminder-3thSatDec1Hour.timer

# list systemd timers
systemctl list-timers
