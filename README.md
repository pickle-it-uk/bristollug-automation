# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
  Simple Python script to send email using Amazon SES.
  Includes systemd units and timers to facilitate scheduling of the.
  Systemd timers included:
  4th Thursday/Friday/Saturday of the Month and Today (1 hours before the meeting time on the 4th Saturday).
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
  * AWS cli V2.
    * IAM user with the following permissions:
      * SendEmail.
      * SendRawEmail.
    * A verified email address.
  * Python 3
    * Python modules:
      * os, shutil, sys, subprocess, argparse, json, io
  * Systemd
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
    * david@bristol.lug.org.uk
* Other community or team contact
